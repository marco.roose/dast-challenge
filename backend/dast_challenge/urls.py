# -*- coding: utf-8 -*-

"""Main URL configuration."""

from django.contrib import admin
from django.urls import path

from DataViewer.api.api import ninja_api as api

urlpatterns = [
    path('api/', api.urls),
    path('admin/', admin.site.urls),
]
