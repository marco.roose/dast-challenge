# -*- coding: utf-8 -*-

"""App config for the DataViewer app."""

from django.apps import AppConfig


class DataviewerConfig(AppConfig):
    """App config for the DataViewer app."""

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'DataViewer'
