# -*- coding: utf-8 -*-

"""Models for the DataViewer app."""

from django.core.validators import MinValueValidator
from django.db import models


class Target(models.Model):
    """Model for the targets."""

    target_id = models.PositiveIntegerField(
        verbose_name='Target ID',
        primary_key=True,
    )
    name = models.CharField(
        verbose_name='name',
        # todo: assumed from sample data
        max_length=10,
        unique=True,
    )

    class Meta:
        ordering = ['target_id']
        verbose_name = 'target'
        verbose_name_plural = 'targets'

    def __str__(self):
        """Return an object string representation."""
        return f'{self.name}'

    def __repr__(self):
        """Return an object representation."""
        return f'Target: {self.name}'


class LightcurveMeasurement(models.Model):
    """Model for a timestamped measurement data point."""

    target = models.ForeignKey(
        to=Target,
        # todo: safest possible
        on_delete=models.PROTECT,
    )

    timestamp = models.FloatField(
        verbose_name='timestamp',
        validators=[
            MinValueValidator(0),
        ],
    )
    value = models.FloatField(  # noqa: WPS110
        verbose_name='value',
        # assuming to be always positive from example data
        validators=[
            MinValueValidator(0),
        ],
    )

    class Meta:
        ordering = ['target__target_id', 'timestamp']
        verbose_name = 'light curve measurement'
        verbose_name_plural = 'light curve measurements'

    def __str__(self):
        """Return an object string representation."""
        return f'{self.value}@{self.timestamp} ({self.target.name})'  # noqa: WPS221

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        """
        Save the current instance.

        Call clean_fields to ensure model field validation.
        """
        self.clean_fields()
        super().save(force_insert, force_update, using, update_fields)

    def __repr__(self):
        """Return an object representation."""
        return f'LightcurveMeasurement: {self.pk}'
