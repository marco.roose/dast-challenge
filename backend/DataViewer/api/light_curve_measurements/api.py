# -*- coding: utf-8 -*-

"""Endpoints for LightcurveMeasurement model objects."""

from typing import List, Optional

from django.db.models import Max, Min
from django.shortcuts import get_object_or_404

from ninja import Router

from DataViewer.api.light_curve_measurements.schemas import (
    LightcurveMeasurementIn,
    LightcurveMeasurementOut,
    LightCurveMeasurementSummaryOut,
)
from DataViewer.models import LightcurveMeasurement, Target

router = Router()


@router.post(
    '',
    summary='Create a light curve measurement',
    url_name='light_curve_measurements',
)
def create_target(request, payload: LightcurveMeasurementIn):
    """Endpoint for creating light curve measurements."""
    target = get_object_or_404(Target, pk=payload.target_pk)
    measurement = LightcurveMeasurement.objects.create(
        target=target,
        timestamp=payload.timestamp,
        value=payload.value,
    )
    return {'id': measurement.pk}


@router.get(
    '',
    summary='Get light curve measurements',
    response=List[LightcurveMeasurementOut],
    url_name='light_curve_measurements',
)
def list_ligthcurvemeasurements(
    request,
    target_pk: Optional[int] = None,
    timestamp_gte: int = 0,
    timestamp_lte: Optional[int] = None,
):
    """Return a list of light curve measurements."""
    queryset = LightcurveMeasurement.objects.all()
    if target_pk:
        target = get_object_or_404(Target, pk=target_pk)
        queryset = queryset.filter(target=target)
    if not timestamp_lte:
        timestamp_lte = queryset.aggregate(Max('timestamp'))['timestamp__max']
    return queryset.filter(timestamp__lte=timestamp_lte, timestamp__gte=timestamp_gte)


@router.get(
    '/summary/',
    summary='Get aggregation light curve measurements [timestamps]',
    response=LightCurveMeasurementSummaryOut,
    url_name='light_curve_measurement_summary',
)
def list_ligthcurvemeasurement_summary(request):
    """
    Return a summary of light curve measurements.

    In the moment only for timestamps, but this could get easily get
    extended for the values, too.
    """
    aggregation = LightcurveMeasurement.objects.aggregate(Max('timestamp'), Min('timestamp'))
    return LightCurveMeasurementSummaryOut(
        min_timestamp=aggregation['timestamp__min'],
        max_timestamp=aggregation['timestamp__max'],
    )


@router.get(
    '/{pk}',
    summary='Get light curve measurement',
    response=LightcurveMeasurementOut,
    url_name='light_curve_measurement',
)
def get_ligthcurvemeasurement(request, pk: int):
    """Return a single light curve measurement."""
    return get_object_or_404(LightcurveMeasurement, pk=pk)


@router.put(
    '/{pk}',
    summary='Update a light curve measurement',
    url_name='light_curve_measurement',
)
def update_target(request, pk: int, payload: LightcurveMeasurementIn):
    """Endpoint for updates of targets."""
    target = get_object_or_404(Target, pk=payload.target_pk)
    measurement = get_object_or_404(LightcurveMeasurement, pk=pk)

    measurement.target = target
    measurement.timestamp = payload.timestamp
    measurement.value = payload.value
    measurement.save()

    return {'success': True}


@router.delete(
    '/{pk}',
    summary='Delete a light curve measurement',
    url_name='light_curve_measurement',
)
def delete_measurement(request, pk: int):
    """Delete light curve measurement via API."""
    measurement = get_object_or_404(LightcurveMeasurement, pk=pk)
    measurement.delete()
    return {'success': True}
