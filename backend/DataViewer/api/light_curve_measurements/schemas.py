# -*- coding: utf-8 -*-

"""Schemas for LightcurveMeasurement objects in the DataViewer API."""

from ninja import Schema
from pydantic.fields import Field

from DataViewer.api.target.schemas import TargetOut


class LightcurveMeasurementOut(Schema):
    """Schema for API output of LightcurveMeasurement objects."""

    target: TargetOut
    timestamp: float
    value: float  # noqa: WPS110


class LightcurveMeasurementIn(Schema):
    """Schema for API input of LightcurveMeasurement model objects."""

    target_pk: int = Field(title='PK of corresponding target')
    timestamp: float
    value: float  # noqa: WPS110


class LightCurveMeasurementSummaryOut(Schema):
    """Schema for API output of summary/aggregations of light curve measurements."""

    min_timestamp: int
    max_timestamp: int
