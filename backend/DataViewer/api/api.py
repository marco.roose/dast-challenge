# -*- coding: utf-8 -*-

"""API root entry point."""

from ninja import NinjaAPI, Router

from DataViewer.api.light_curve_measurements.api import router as measurements_router
from DataViewer.api.target.api import router as targets_router

ninja_api = NinjaAPI(
    title='DAST Challenge: data viewer API',
    version='0.1',
    description='RESTful API for the data viewer model data',
    urls_namespace='api',
)


base_router = Router()
ninja_api.add_router('dataviewer', base_router)

# add name spaces for targets and light curve measurements
base_router.add_router('targets', targets_router, tags=['Targets'])
base_router.add_router('ligthcurvemeasurements', measurements_router, tags=['Light curve measurements'])
