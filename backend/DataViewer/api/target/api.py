# -*- coding: utf-8 -*-

"""Endpoints for Target model objects."""

from typing import List

from django.shortcuts import get_object_or_404

from ninja import Router

from DataViewer.api.target.schemas import TargetIn, TargetOut
from DataViewer.models import Target

router = Router()


@router.post(
    '',
    summary='Create a target',
    url_name='targets',
)
def create_target(request, payload: TargetIn):
    """Endpoint for creating targets."""
    target = Target.objects.create(**payload.dict())
    return {'id': target.pk}


@router.get(
    '/{target_pk}',
    summary='Get single target',
    response=TargetOut,
    url_name='target',
)
def get_target(request, target_pk: int):
    """Return a single targets."""
    return get_object_or_404(Target, pk=target_pk)


@router.get(
    '',
    summary='Get all targets',
    response=List[TargetOut],
    url_name='targets',
)
def list_targets(request):
    """Return a list of targets."""
    return Target.objects.all()


@router.put(
    '/{target_pk}',
    summary='Update a target',
    url_name='target',
)
def update_target(request, target_pk: int, payload: TargetIn):
    """Endpoint for updates of targets."""
    target = get_object_or_404(Target, pk=target_pk)
    for field_name, new_value in payload.dict().items():
        setattr(target, field_name, new_value)
    target.save()
    return {'success': True}


@router.delete(
    '/{target_pk}',
    summary='Delete a target',
    url_name='target',
)
def delete_target(request, target_pk: int):
    """Delete a target via API."""
    target = get_object_or_404(Target, pk=target_pk)
    target.delete()
    return {'success': True}
