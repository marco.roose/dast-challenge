# -*- coding: utf-8 -*-

"""Schemas for Target objects in the DataViewer API."""

from ninja import Schema


class TargetOut(Schema):
    """Schema for API output of Target model objects."""

    target_id: int
    name: str


class TargetIn(TargetOut):
    """
    Schema for API input of Target model objects.

    Following the doc create twi schemas for input and output.
    As the not differ just re-use the TargetOut schema.
    """
