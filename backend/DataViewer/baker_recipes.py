# -*- coding: utf-8 -*-

"""Baker recipes to create model instances for tests."""

from mimesis import Numbers, Text
from model_bakery.recipe import Recipe, foreign_key

from DataViewer.models import LightcurveMeasurement, Target


def get_timestamp():
    """
    Get a timestamp for the LightcurveMeasurement model factory.

    We expect a float between 0.0 and 100.0 with single digit precision.
    """
    return Numbers().float_number(start=0, end=100, precision=1)


def get_measurement_value():
    """
    Get a value for the LightcurveMeasurement model factory.

    We expect a float between 0.0 and 100.0 with multiple digit precision.
    """
    return Numbers().float_number(start=0, end=100, precision=20)  # noqa: WPS432


target = Recipe(
    Target,
    name=Text('en').word,
)

light_curve_measurement = Recipe(
    LightcurveMeasurement,
    target=foreign_key(target),
    timestamp=get_timestamp,
    value=get_measurement_value,
)
