# -*- coding: utf-8 -*-

"""Management command to import sample data."""

import csv
from typing import Any

from django.conf import settings
from django.core.management.base import BaseCommand

from DataViewer.models import LightcurveMeasurement, Target


class SemicolonDialect(csv.excel):
    """CSV dialect wit semicolon as field delimiter."""

    delimiter = ';'


def get_or_create_target(target_id: int, name: str) -> Target:
    """Get or create a Target object."""
    target, created = Target.objects.get_or_create(target_id=target_id, name=name)
    return target


class Command(BaseCommand):
    """Management command to import sample data."""

    def handle(self, *args: Any, **options: Any):  # noqa: WPS110
        """Import sample CSV to database."""
        with open(settings.BASE_DIR / 'sample_dataset.csv', 'r', newline='') as sample_file:
            sample_data = csv.DictReader(sample_file, dialect=SemicolonDialect)

            for measurement in sample_data:
                target = get_or_create_target(
                    target_id=measurement['targetid'],
                    name=measurement['name'],
                )
                LightcurveMeasurement.objects.create(
                    target=target,
                    timestamp=measurement['timestamp'],
                    value=measurement['value'],
                )
