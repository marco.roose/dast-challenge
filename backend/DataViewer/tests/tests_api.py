# -*- coding: utf-8 -*-

"""Tests for the REST API of the DataViewer app."""
import json
from typing import List

from django.urls import reverse

from mimesis import Numbers, Text
from model_bakery.baker import make_recipe
from test_plus import TestCase

from DataViewer.api.light_curve_measurements.schemas import LightcurveMeasurementOut
from DataViewer.api.target.schemas import TargetOut
from DataViewer.models import LightcurveMeasurement, Target


class TargetAPITestCase(TestCase):
    """Tests for the API for Target model objects."""

    baker_model_string = 'DataViewer.target'
    url_namespace = 'api:target'

    def get_target_dict(self, target_object: Target) -> dict:
        """Serialize a Target object using API schema."""
        return TargetOut.from_orm(target_object).dict()

    def test_get_targets(self):
        """Test resource for target list."""
        targets = [
            make_recipe(self.baker_model_string),
            make_recipe(self.baker_model_string),
        ]

        response = self.get(f'{self.url_namespace}s')

        self.assert_http_200_ok(response)

        json_response = response.json()

        self.assertEqual(
            len(targets),
            len(json_response),
            f'Expect the API response list to have a length of {len(targets)}',
        )

        for target in targets:
            with self.subTest(f'Check for target "{target}" in response'):
                self.assertIn(
                    self.get_target_dict(target),
                    json_response,
                )

    def test_get_target(self):
        """Test resource for single targets."""
        target = make_recipe(self.baker_model_string)

        response = self.get(reverse(f'{self.url_namespace}', args=[target.pk]))

        self.assert_http_200_ok(response)

        json_response = response.json()

        self.assertDictEqual(
            self.get_target_dict(target),
            json_response,
        )

    def test_create_target(self):
        """Test create a target via API."""
        target_args = {
            'target_id': Numbers().integer_number(10, 20),
            'name': Text().word(),
        }
        response = self.client.post(
            reverse(f'{self.url_namespace}s'),
            json.dumps(target_args),
            content_type='application/json',
        )
        self.assert_http_200_ok(response)

        created_target = Target.objects.get(**target_args)

        self.assertDictEqual(
            response.json(),
            {'id': created_target.pk},
        )
        self.assertEqual(
            created_target.name,
            target_args['name'],
        )
        self.assertEqual(
            created_target.target_id,
            target_args['target_id'],
        )

    def test_update_target(self):
        """Test update a target via API."""
        test_target: Target = make_recipe('DataViewer.target')
        target_update_args = {
            'target_id': test_target.target_id,
            'name': Text().word(),
        }
        response = self.client.put(
            reverse(f'{self.url_namespace}', args=[test_target.pk]),
            json.dumps(target_update_args),
            content_type='application/json',
        )
        self.assert_http_200_ok(response)
        self.assertDictEqual(
            response.json(),
            {'success': True},
        )
        test_target.refresh_from_db()

        self.assertEqual(
            test_target.name,
            target_update_args['name'],
        )

    def test_delete_target(self):
        """Test delete Target via API."""
        test_target: Target = make_recipe('DataViewer.target')

        response = self.delete(
            reverse(f'{self.url_namespace}', args=[test_target.pk]),
        )
        self.assert_http_200_ok(response)
        self.assertDictEqual(
            response.json(),
            {'success': True},
        )
        with self.assertRaises(Target.DoesNotExist):
            test_target.refresh_from_db()


class LightcurveMeasurementAPITestCase(TestCase):  # noqa: WPS214
    """Tests for the API for LightcurveMeasurement model objects."""

    baker_model_string = 'DataViewer.light_curve_measurement'
    url_namespace = 'api:light_curve_measurement'

    @classmethod
    def setUpTestData(cls):
        """Prepare some test data."""
        cls.target = make_recipe('DataViewer.target')

    def get_lightcurve_measurement_dict(self, measurement_object: LightcurveMeasurement) -> dict:
        """Serialize a LightcurveMeasurement object using API schema."""
        return LightcurveMeasurementOut.from_orm(measurement_object).dict()

    def test_get_lightcurve_measurements(self):
        """Test resource for light curve measurement list."""
        measurements = [
            make_recipe(self.baker_model_string, target=self.target),
            make_recipe(self.baker_model_string, target=self.target),
        ]

        response = self.get(f'{self.url_namespace}s')

        self.assert_http_200_ok(response)

        json_response = response.json()

        self.assertEqual(
            len(measurements),
            len(json_response),
            f'Expect the API response list to have a length of {len(measurements)}',
        )

        for measurement in measurements:
            with self.subTest(f'Check for measurement "{measurement}" in response'):
                self.assertIn(
                    self.get_lightcurve_measurement_dict(measurement),
                    json_response,
                )

    def test_get_light_curve_measurement(self):
        """Test resource for single light curve measurements."""
        measurement = make_recipe(self.baker_model_string, target=self.target)

        response = self.get(reverse(f'{self.url_namespace}', args=[measurement.pk]))

        self.assert_http_200_ok(response)

        json_response = response.json()

        self.assertDictEqual(
            self.get_lightcurve_measurement_dict(measurement),
            json_response,
        )

    def test_get_light_curve_measurement_by_target(self):
        """Test API query of measurements filtered by target."""
        another_target = make_recipe('DataViewer.target')
        measurement_default_target = make_recipe(self.baker_model_string, target=self.target)
        measurement_another_target = make_recipe(self.baker_model_string, target=another_target)

        response = self.client.get(
            reverse(f'{self.url_namespace}s'),
            data={
                'target_pk': another_target.pk,
            },
        )

        self.assert_http_200_ok(response)

        json_response = response.json()

        self.assertNotIn(
            self.get_lightcurve_measurement_dict(measurement_default_target),
            json_response,
        )

        self.assertIn(
            self.get_lightcurve_measurement_dict(measurement_another_target),
            json_response,
        )

    def test_get_light_curve_measurement_by_timestamp(self):
        """Test API query of measurements filtered for timestamp."""
        timestamps = [1, 2, 3, 4, 5]
        for timestamp in timestamps:
            make_recipe(
                self.baker_model_string,
                target=self.target,
                timestamp=timestamp,
            )

        with self.subTest('Borders out of range'):
            response = self.client.get(
                reverse(f'{self.url_namespace}s'),
                data={
                    'timestamp_gte': timestamps[0] - 1,
                    'timestamp_lte': timestamps[-1] + 1,
                },
            )

            self.assert_http_200_ok(response)

            json_response = response.json()

            self.assertEqual(
                len(timestamps),
                len(json_response),
            )

        with self.subTest('Borders exactly in range'):
            response = self.client.get(
                reverse(f'{self.url_namespace}s'),
                data={
                    'timestamp_gte': timestamps[0],
                    'timestamp_lte': timestamps[-1],
                },
            )
            json_response = response.json()
            self.assertEqual(
                len(timestamps),
                len(json_response),
            )

        with self.subTest('First timestamp excluded'):
            response = self.client.get(
                reverse(f'{self.url_namespace}s'),
                data={
                    'timestamp_gte': timestamps[1],
                    'timestamp_lte': timestamps[-1],
                },
            )
            json_response = response.json()
            self.assertEqual(
                len(timestamps) - 1,
                len(json_response),
            )
            self.assertNotIn(
                self.get_lightcurve_measurement_dict(
                    LightcurveMeasurement.objects.get(timestamp=timestamps[0]),
                ),
                json_response,
            )

        with self.subTest('Last timestamp excluded'):
            response = self.client.get(
                reverse(f'{self.url_namespace}s'),
                data={
                    'timestamp_gte': timestamps[0],
                    'timestamp_lte': timestamps[-2],
                },
            )
            json_response = response.json()
            self.assertEqual(
                len(timestamps) - 1,
                len(json_response),
            )
            self.assertNotIn(
                self.get_lightcurve_measurement_dict(
                    LightcurveMeasurement.objects.get(timestamp=timestamps[-1]),
                ),
                json_response,
            )

    def test_create_measurement(self):
        """Test create a light curve measurement via API."""
        measurement_args = {
            'target_pk': self.target.pk,
            'timestamp': Numbers().float_number(10, 20),
            'value': Numbers().float_number(0, 10),
        }
        response = self.client.post(
            reverse(f'{self.url_namespace}s'),
            json.dumps(measurement_args),
            content_type='application/json',
        )
        self.assert_http_200_ok(response)

        created_measurement = LightcurveMeasurement.objects.get(
            target=self.target,
            timestamp=measurement_args['timestamp'],
            value=measurement_args['value'],
        )

        self.assertDictEqual(
            response.json(),
            {'id': created_measurement.pk},
        )

    def test_update_measurement(self):
        """Test update a light curve measurement via API."""
        test_measurement: LightcurveMeasurement = make_recipe('DataViewer.light_curve_measurement')
        measurement_update_args = {
            'target_pk': test_measurement.target_id,
            'timestamp': test_measurement.timestamp,
            'value': Numbers().float_number(10, 20),
        }

        response = self.client.put(
            reverse(f'{self.url_namespace}', args=[test_measurement.pk]),
            json.dumps(measurement_update_args),
            content_type='application/json',
        )

        self.assert_http_200_ok(response)
        self.assertDictEqual(
            response.json(),
            {'success': True},
        )
        test_measurement.refresh_from_db()

        self.assertEqual(
            test_measurement.value,
            measurement_update_args['value'],
        )

    def test_delete_measurement(self):
        """Test delete light curve measurement via API."""
        test_measurement: LightcurveMeasurement = make_recipe(
            'DataViewer.light_curve_measurement',
        )

        response = self.delete(
            reverse(f'{self.url_namespace}', args=[test_measurement.pk]),
        )
        self.assert_http_200_ok(response)
        self.assertDictEqual(
            response.json(),
            {'success': True},
        )
        with self.assertRaises(LightcurveMeasurement.DoesNotExist):
            test_measurement.refresh_from_db()


class LightcurveMeasurementSummaryAPITestCase(TestCase):
    """Tests for the aggregation endpoint for LightcurveMeasurements."""

    baker_model_string = 'DataViewer.light_curve_measurement'
    url_namespace = 'api:light_curve_measurement'

    @classmethod
    def setUpTestData(cls):
        """Prepare some test data."""
        cls.target = make_recipe('DataViewer.target')

    def create_measurements(self, target: Target, timestamps: List[int]) -> None:
        """Create a time row of measurements."""
        for timestamp in timestamps:
            make_recipe(
                self.baker_model_string,
                target=target,
                timestamp=timestamp,
            )

    def test_get_light_curve_measurements_summary(self):
        """Test summary resource for light curve measurements."""
        timestamps = [1, 2, 3, 4, 5, 6]
        self.create_measurements(target=self.target, timestamps=timestamps)

        response = self.get(f'{self.url_namespace}_summary')
        self.assert_http_200_ok(response)

        self.assertDictEqual(
            {
                'min_timestamp': timestamps[0],
                'max_timestamp': timestamps[-1],
            },
            response.json(),
        )
