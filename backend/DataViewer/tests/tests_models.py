# -*- coding: utf-8 -*-

"""Tests for the models of the DataViewer app."""

from django.core.exceptions import ValidationError

from model_bakery.baker import make_recipe
from test_plus import TestCase

from DataViewer.models import LightcurveMeasurement, Target


class TargetModelTestCase(TestCase):
    """Tests for the Target model."""

    @classmethod
    def setUpTestData(cls):
        """Prepare some test data."""
        cls.target = make_recipe('DataViewer.target')

    def test_str_representation(self):
        """Test the string representation of the model."""
        self.assertEqual(
            str(self.target),
            self.target.name,
        )

    def test_repr_representation(self):
        """Test the object representation of the model."""
        self.assertEqual(
            repr(self.target),
            f'Target: {self.target.name}',
        )


class LightcurveMeasurementModelTestCase(TestCase):
    """Tests for the Target model."""

    @classmethod
    def setUpTestData(cls):
        """Prepare some test data."""
        cls.target: Target = make_recipe('DataViewer.target')
        cls.measurement: LightcurveMeasurement = make_recipe(
            'DataViewer.light_curve_measurement',
            target=cls.target,
        )

    def test_str_representation(self):
        """Test the string representation of the model."""
        self.assertEqual(
            str(self.measurement),
            f'{self.measurement.value}@{self.measurement.timestamp} ({self.target.name})',  # noqa: WPS221
        )

    def test_repr_representation(self):
        """Test the object representation of the model."""
        self.assertEqual(
            repr(self.measurement),
            f'LightcurveMeasurement: {self.measurement.pk}',
        )

    def test_timestamp_field_validation(self):
        """Test validation of the timestamp field."""
        with self.subTest('negative values are not allowed'):
            expected_message = "{'timestamp': ['Ensure this value is greater than or equal to 0.']}"
            with self.assertRaisesMessage(ValidationError, expected_message):
                self.measurement.timestamp = -1.0
                self.measurement.save()

    def test_value_field_validation(self):
        """Test validation of the value field."""
        with self.subTest('negative values are not allowed'):
            expected_message = "{'value': ['Ensure this value is greater than or equal to 0.']}"
            with self.assertRaisesMessage(ValidationError, expected_message):
                self.measurement.value = -1.0
                self.measurement.save()
