# -*- coding: utf-8 -*-

"""Model admins for the DataViewer app."""

from django.contrib import admin

from DataViewer.models import LightcurveMeasurement, Target


@admin.register(Target)
class TargetAdmin(admin.ModelAdmin):
    """Admin for the Target model."""

    list_display = ('target_id', 'name')


@admin.register(LightcurveMeasurement)
class LightcurveMeasurementAdmin(admin.ModelAdmin):
    """Admin for the LightcurveMeasurement model."""

    list_display = ('target', 'timestamp', 'value')
