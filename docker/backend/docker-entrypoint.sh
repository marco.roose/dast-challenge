#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

echo "Migrating the db..."
python manage.py migrate

echo "Filling database with sample data"
python manage.py flush --noinput
python manage.py migrate
python manage.py import_example_data

if [ ${DJANGO_DEBUG:=0} -ne 1 ]
then
  echo "Collecting static files into STATIC_ROOT..."
  python manage.py collectstatic --noinput
fi

echo "Running any commands passed to this script from CMD of command..."
exec "$@"
