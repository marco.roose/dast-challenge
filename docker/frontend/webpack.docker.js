const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.dev.js');
const webpack = require('webpack');

module.exports = merge(common, {
  mode: 'development',
  devServer: {
    proxy: {
      '/api/**': {
        target: 'http://backend:8000',
        changeOrigin: true,
        secure: false,
      },
    },
  },
});
