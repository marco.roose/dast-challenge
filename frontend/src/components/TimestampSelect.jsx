import React, { Component, Fragment } from "react";
import axios from "axios";
import { MEASUREMENT_SUMMARY_API_URL } from "./Constants";
import { Label, Input, Col } from "reactstrap";

class TimeStampRangeSelect extends Component {
  constructor(props) {
    super(props);
    this.handleLowerTimestampChange =
      this.handleLowerTimestampChange.bind(this);
    this.handleUpperTimestampChange =
      this.handleUpperTimestampChange.bind(this);
    this.state = {
      min_timestamp: 0,
      max_timestamp: 100,
    };
  }

  componentDidMount() {
    this.getTimestampMinAndMax();
  }

  cleanup_timestamp_value(timestamp_value) {
    // ensure values do not "cross the borders"
    let cleaned_timestamp_value = timestamp_value;
    if (timestamp_value < this.state.min_timestamp) {
      cleaned_timestamp_value = this.state.min_timestamp;
    }

    if (timestamp_value > this.state.max_timestamp) {
      cleaned_timestamp_value = this.state.max_timestamp;
    }
    return cleaned_timestamp_value;
  }

  handleLowerTimestampChange = (e) => {
    let lower_timestamp = this.cleanup_timestamp_value(e.target.value);
    this.props.timestamp_lower = lower_timestamp;
    this.props.onLowerTimestampChange(lower_timestamp);
  };

  handleUpperTimestampChange = (e) => {
    let upper_timestamp = this.cleanup_timestamp_value(e.target.value);
    this.props.timestamp_upper = upper_timestamp;
    this.props.onUpperTimestampChange(upper_timestamp);
  };

  getTimestampMinAndMax = () => {
    // for initial setup of the "filter"
    // used an API call as one of the possible methods
    // advantage: client CPU power is saved and on the
    // server caching would be possible if needed.
    // not taking selected target into account in the moment,
    // had some problems implementing this ;-)

    axios
      .get(MEASUREMENT_SUMMARY_API_URL)
      .then((res) => {
        this.setState({ min_timestamp: res.data.min_timestamp });
        this.setState({ max_timestamp: res.data.max_timestamp });
        this.props.max_timestamp = res.data.max_timestamp;
      })
      .catch((err) => console.log(err));
  };

  render() {
    let lower_timestamp = this.props.timestamp_lower;
    let upper_timestamp = this.props.timestamp_upper;

    return (
      <Fragment>
        <Col className="col-lg-2 col-md-3">
          <Label for="timestamp_lower">Timestamp lower</Label>
        </Col>
        <Col className="col-2">
          <Input
            type="number"
            name="timestamp_lower"
            id="timestamp_lower"
            min={this.state.min_timestamp}
            max={this.state.max_timestamp}
            value={lower_timestamp}
            onChange={this.handleLowerTimestampChange}
          />
        </Col>
        <Col className="col-lg-2 col-md-3">
          <Label for="timestamp_upper">Timestamp upper</Label>
        </Col>
        <Col className="col-2">
          <Input
            type="number"
            name="timestamp_upper"
            id="timestamp_end"
            min={this.state.min_timestamp}
            max={this.state.max_timestamp}
            value={upper_timestamp || this.state.max_timestamp}
            onChange={this.handleUpperTimestampChange}
          />
        </Col>
      </Fragment>
    );
  }
}

export default TimeStampRangeSelect;