import {Col, FormGroup, Input, Row} from "reactstrap";
import React, {Component, Fragment} from "react";

class TargetSelect extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    renderTargetOptions(targets) {
        return (
            targets.map((target) => (
                    <option key={target.target_id} value={target.target_id}>
                        {target.name}
                    </option>
                )
            )
        )
    }

    handleChange(e) {
        this.props.onTargetChange(e.target.value);
    }

    render() {
        let target_options = ""

        if (this.props.targets.length > 0) {
            target_options = <Fragment>
                <Col className="col-lg-1 col-md-2">
                    <label form="target">Target</label>
                </Col>
                <Col className="col-3">
                    <Input type="select" id="target" name="target" onChange={this.handleChange}>
                        <option key="0" value="">---</option>
                        {this.renderTargetOptions(this.props.targets)}
                    </Input>
                </Col>
            </Fragment>
        }
        return target_options
    }
}

export default TargetSelect;