export const MEASUREMENT_API_URL = "/api/dataviewer/ligthcurvemeasurements";
export const MEASUREMENT_SUMMARY_API_URL =
  "/api/dataviewer/ligthcurvemeasurements/summary/";
export const TARGET_API_URL = "api/dataviewer/targets";
