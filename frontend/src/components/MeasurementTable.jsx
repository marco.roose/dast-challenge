import { Component, Fragment } from "react";
import { Table, Progress } from "reactstrap";

class MeasurementTable extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const measurements = this.props.measurements;
    const max_value = this.props.max_value;
    return (
      <Fragment>
        <Table>
          <thead>
            <tr>
              <th className="col-2">Target</th>
              <th className="col-2">timestamp</th>
              <th className="col-3">value</th>
              <th className="col-5">% max value</th>
            </tr>
          </thead>
          <tbody>
            {!measurements || measurements.length <= 0 ? (
              <tr>
                <td colSpan="6" align="center">
                  <b>Ops, no measurements, yet</b>
                </td>
              </tr>
            ) : (
              measurements.map((measurement) => (
                <tr key={measurement.pk}>
                  <td>{measurement.target.name}</td>
                  <td>{measurement.timestamp}</td>
                  <td>{measurement.value}</td>
                  <td>
                    <Progress value={measurement.value} max={max_value} />
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </Table>
      </Fragment>
    );
  }
}

export default MeasurementTable;
