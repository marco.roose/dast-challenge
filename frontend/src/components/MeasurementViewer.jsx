import { Component, Fragment } from "react";
import MeasurementTable from "./MeasurementTable";
import TargetSelect from "./TargetSelect";
import axios from "axios";
import { TARGET_API_URL, MEASUREMENT_API_URL } from "./Constants";
import { Card, CardHeader, CardBody, Row } from "reactstrap";
import TimeStampRangeSelect from "./TimestampSelect";

class MeasurementViewer extends Component {
  constructor(props) {
    super(props);
    this.setCurrentTarget = this.setCurrentTarget.bind(this);
    this.setLowerTimestamp = this.setLowerTimestamp.bind(this);
    this.setUpperTimestamp = this.setUpperTimestamp.bind(this);

    this.min_timestamp = 0;
    this.max_timestamp = undefined;

    this.state = {
      measurements: [],
      targets: [],
      selected_target: undefined,
      timestamp_lower: this.min_timestamp,
      timestamp_upper: this.max_timestamp,
      max_value: 0,
    };
  }

  componentDidMount() {
    this.getMeasurements();
    this.getTargets();
    // regularly refresh targets to get newly created targets
    this.getTargetsID = setInterval(() => this.getTargets(), 10000);

    // regularly refresh measurements to get newly created measurements
    this.getMeasurementsID = setInterval(
      () =>
        this.getMeasurements(
          this.state.selected_target,
          this.state.timestamp_lower,
          this.state.timestamp_upper
        ),
      10000
    );
  }

  componentWillUnmount() {
    clearInterval(this.getTargetsID);
    clearInterval(this.getMeasurementsID);
  }

  getTargets = () => {
    // get the targets to setup the target select
    axios
      .get(TARGET_API_URL)
      .then((res) => this.setState({ targets: res.data }))
      .catch((err) => console.log(err));
  };

  getMeasurements = (target_pk, timestamp_lower, timestamp_upper) => {
    // get the measurements via API call
    // taking in account all the filter input
    let options = {
      params: {
        timestamp_gte: timestamp_lower,
        timestamp_lte: timestamp_upper,
      },
    };
    if (target_pk) {
      options.params.target_pk = target_pk || this.state.selected_target;
    }
    axios
      .get(MEASUREMENT_API_URL, options)
      .then((res) => {
        let measurements = res.data;
        this.setState({ measurements: measurements });
        this.getMaxMeasurementValue(measurements);
      })
      .catch((err) => console.log(err));
  };

  getMaxMeasurementValue = (measurements) => {
    // for the visualization of the values we need the maximum
    // could have been done with an API call like for the timestamp
    // but just wanted to present both possible methods
    let max_value = 0;
    let current_value;
    for (let measurement_idx in measurements || this.state.measurements) {
      current_value = measurements[measurement_idx].value;
      if (current_value > max_value) {
        max_value = current_value;
      }
    }
    this.setState({ max_value: max_value });
  };

  setCurrentTarget = (e) => {
    // update measurements on change of target filter
    if (e === "") {
      e = undefined;
    }
    this.setState({ selected_target: e });

    this.getMeasurements(
      e,
      this.state.timestamp_lower,
      this.state.timestamp_upper
    );
  };

  setLowerTimestamp = (e) => {
    // empty or none values do not make sense
    // prevent lower timestamp from getting empty
    // assuming 0 to be the lowest value will thus disable filtering for lowest value
    if (!e || e === "") {
      e = this.min_timestamp;
    }

    // update state and measurements
    this.setState({ timestamp_lower: e });
    this.getMeasurements(
      this.state.selected_target,
      e,
      this.state.timestamp_upper
    );
  };

  setUpperTimestamp = (e) => {
    // empty or none values do not make sense
    // prevent upper timestamp from getting empty
    // in that case just take global maximum which is same as disabling upper filter
    if (!e || e === "") {
      e = this.max_timestamp;
    }

    // update state and measurements
    this.setState({ timestamp_upper: e });
    this.getMeasurements(
      this.state.selected_target,
      this.state.timestamp_lower,
      e
    );
  };

  render() {
    return (
      <Fragment>
        <Card>
          <CardHeader>
            <h4>Filter</h4>
            <Row>
              <TargetSelect
                targets={this.state.targets}
                onTargetChange={this.setCurrentTarget}
              />
              <TimeStampRangeSelect
                timestamp_lower={this.state.timestamp_lower}
                timestamp_upper={this.state.timestamp_upper}
                handleTargetChange={this.state.selected_target}
                onLowerTimestampChange={this.setLowerTimestamp}
                onUpperTimestampChange={this.setUpperTimestamp}
              />
            </Row>
          </CardHeader>
          <CardBody>
            <MeasurementTable
              measurements={this.state.measurements}
              max_value={this.state.max_value}
            />
          </CardBody>
        </Card>
      </Fragment>
    );
  }
}

export default MeasurementViewer;
