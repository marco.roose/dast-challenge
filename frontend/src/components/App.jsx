import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import React from "react";
import { Container, Card, CardHeader } from "reactstrap";
import MeasurementViewer from "./MeasurementViewer";

const App = () => (
  <Router>
    <Switch>
      <Route exact path="/">
        <Container>
          <Card id="header" className="mt-2 mb-4">
            <CardHeader className="bg-light">
              <h2 className="display4">DAST Developer Challenge: Data Viewer</h2>
              <hr className="mr-4" />
              <p>
                Filter and display light curve measurement data by target and
                timestamp.
              </p>
            </CardHeader>
          </Card>
          <MeasurementViewer />
        </Container>
      </Route>
    </Switch>
  </Router>
);

export default App;
