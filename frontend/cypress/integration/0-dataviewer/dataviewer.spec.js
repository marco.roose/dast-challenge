/// <reference types="cypress" />

/*
 * just basic integration tests of the main functions of the app
 * should maybe get improved by some unit tests
 */

import {
  MEASUREMENT_API_URL,
  MEASUREMENT_SUMMARY_API_URL,
  TARGET_API_URL,
} from "../../../src/components/Constants";

describe("integration tests for the DAST data viewer", () => {
  beforeEach(() => {
    cy.intercept(TARGET_API_URL, {
      fixture: "targets.json",
    }).as("all_targets");

    cy.intercept(MEASUREMENT_API_URL, {
      fixture: "measurements.json",
    }).as("all_measurements");

    cy.intercept(MEASUREMENT_SUMMARY_API_URL, {
      fixture: "measurement_summary.json",
    }).as("measurement_summary");

    cy.visit("/");
  });

  it("displays the header", () => {
    cy.get("#header h2")
      .first()
      .should("have.text", "DAST Developer Challenge: Data Viewer");
  });

  it("initially displays all measurements", () => {
    cy.wait(1);
    cy.get("table tbody").find("tr").should("have.length", 6);
  });

  it("can filter for target 'bar'", () => {
    cy.intercept(MEASUREMENT_API_URL + "?timestamp_gte=0&target_pk=2", {
      fixture: "measurements_bar.json",
    }).as("bar_measurements");
    cy.get("select").first().select("bar");
    cy.get("table").should("not.contain", "foo");
    cy.get("table").should("contain", "bar");
  });

  it("correctly sets initial timestamp values", () => {
    cy.get('input[name="timestamp_lower"]').should("have.value", 0);
    cy.get('input[name="timestamp_upper"]').should("have.value", 2);
  });

  it("can filter for a lower timestamp", () => {
    cy.intercept(MEASUREMENT_API_URL + "?timestamp_gte=0&target_pk=2", {
      fixture: "measurements_bar.json",
    }).as("bar_measurements");
    cy.intercept(MEASUREMENT_API_URL + "?timestamp_gte=1&target_pk=2", {
      fixture: "measurements_bar_1_to_end.json",
    }).as("bar_measurements_one_to_end");
    // select target bar and lower timestamp 1
    cy.get("select").first().select("bar");
    cy.get('input[name="timestamp_lower"]').type("{selectall}1");

    // check expectations
    cy.get("table tbody").find("tr").should("have.length", 2);
    cy.get("table").should("not.contain", "0");
    cy.get("table").should("contain", "1");
    cy.get("table").should("contain", "2");
  });

  it("can filter for an upper timestamp", () => {
    cy.intercept(MEASUREMENT_API_URL + "?timestamp_gte=0&target_pk=2", {
      fixture: "measurements_bar.json",
    }).as("bar_measurements");
    cy.intercept(
      MEASUREMENT_API_URL + "?timestamp_gte=0&timestamp_lte=1&target_pk=2",
      {
        fixture: "measurements_bar_beginning_to_1.json",
      }
    ).as("bar_measurements_one_to_end");
    // select target bar and lower timestamp 1
    cy.get("select").first().select("bar");
    cy.get('input[name="timestamp_upper"]').type("{selectall}1");

    // check expectations
    cy.get("table tbody").find("tr").should("have.length", 2);
    cy.get("table").should("contain", "0");
    cy.get("table").should("contain", "1");
    cy.get("table").should("not.contain", "2");
  });
});
