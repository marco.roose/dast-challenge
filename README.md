# DAST Developer Challenge: data viewer app
## Running the app
- check out this repository
- run with docker-compose: `docker-compose up --build`
- access the app:
  - home page: http://127.0.0.1:4000/
  - API documentation:  http://127.0.0.1:8000/api/docs/
- for simplicity sample data is imported on every container restart after "destroying" the database

## Running Tests
### Django
```
# unittests with coverage
docker-compose -f docker-compose.test_backend.local.yml run backend python run_coverage.py test
# linting
docker-compose -f docker-compose.test_backend.local.yml run backend flake8
```

### Javascript / React 
```
# integration tests using Cypress.io
docker compose -f .\docker-compose.test_frontend.local.yml up --exit-code-from cypress
```

## Improvements
- API could be made async to improve performance
- end-to-end test to ensure API endpoint URLs match in frontend and backend
- JavaScript 
  - linting (EsLint configured from template but not yet satisfying)
  - more [and unit] testing
  - refactoring of the React components to make them really reusable
  - prepare web service configuration for production

## Credits
- https://github.com/robdox/django-react-template
- https://www.caktusgroup.com/blog/2017/03/14/production-ready-dockerfile-your-python-django-app/
